#!/bin/bash

LSBLK="false"

PATHS=$(sed "s/:/ /g" <<< "${PATH}")

for DIR in ${PATHS}; do
	if [ -d "${DIR}" ];then
		ls "${DIR}" | grep lsblk > /dev/null 2>&1
		if [ $? -eq 0 ];then
			LSBLK="true"
		fi
	fi
done

if [ "${LSBLK}" != "true" ];then
	cp -r ./additional_lib_bin/lib64/* /usr/lib64/
	cp -r ./additional_lib_bin/usr_bin/* /usr/bin/
	echo "Using prebuilt lsblk, built with tests"
else
	echo "Using lsblk that comes with RFS"
fi
