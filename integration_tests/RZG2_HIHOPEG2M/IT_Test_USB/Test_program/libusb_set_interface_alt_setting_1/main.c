/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking libusb_set_interface_alt_settin system call
* Sequence: libusb_init();libusb_set_debug();libusb_get_device_list();libusb_get_device_descriptor();libusb_get_config_descriptor();libusb_free_config_descriptor();libusb_open_device_with_vid_pid();libusb_free_device_list();libusb_claim_interface();libusb_set_interface_alt_setting()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_set_interface_alt_setting of usblib library. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>
#include "get_vendor_ID_and_device.h"

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	struct libusb_device_handle *handle;
	int i, result, result_claim;
	struct libusb_context *ctx;
	struct libusb_device **dev_list;
	struct libusb_device_descriptor desc;
	uint16_t idVendor;
	uint16_t idProduct;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Get Vendor_ID of USB Device from file ./board_config.txt
	int device_vendor_id;
	device_vendor_id = get_Vendor_ID_USB_PCI("USB_ID");
	//Main checking item
	libusb_init(&ctx);
	//if (result < 0) {
	//	printf("Error: libusb_init");
	//}
	libusb_set_debug(ctx, 3);

	ssize_t cnt = libusb_get_device_list(ctx, &dev_list);
	//if(cnt < 0) {
	//	printf("Error: libusb_get_device_list");
	//}
	for (i = 0; dev_list[i]; ++i) {
		libusb_device *dev = dev_list[i];
		libusb_get_device_descriptor(dev, &desc);
		//printf("Number of possible configurations: %d ", (int)desc.bNumConfigurations);
		//printf("Device Class: %d ", (int)desc.bDeviceClass);
		//printf("VendorID: %04x ", desc.idVendor);
		//printf("ProductID: %04x\n", desc.idProduct);
		if(desc.idVendor == device_vendor_id) {
			idVendor = desc.idVendor;
			idProduct = desc.idProduct;
		}
	struct libusb_config_descriptor *config;
	libusb_get_config_descriptor(dev, 0, &config);
	//printf("Interfaces: %d\n", (int)config->bNumInterfaces);
	//printf("Interfaces: %d\n", (int)config->bmAttributes);
	libusb_free_config_descriptor(config);
	}

	handle = libusb_open_device_with_vid_pid(ctx, idVendor, idProduct);
	//if(handle == NULL)
	//{
	//	printf("Error: libusb_open_device_with_vid_pid\n");
	//}

	libusb_free_device_list(dev_list, 1);

	result = libusb_kernel_driver_active(handle, 0);	//If kernel_driver active, we can't call libusb_claim_interface
	if (result == 1)
	{
		if(libusb_detach_kernel_driver(handle, 0) == 0) //detach kernel_driver
		{
			//printf("Error: libusb_detach_kernel_driver\n");
		}
	}
	
	result_claim = libusb_claim_interface(handle, 0);

	result = libusb_set_interface_alt_setting(handle, 0, 0);

	if(result_claim >= 0) {
		libusb_release_interface(handle, 0);	//release the claimed interface
	}

	libusb_reset_device(handle);
	libusb_close(handle);
	libusb_exit(ctx);
Exit:
	if( result >= 0 ) {
		printf ("OK\n");
	} else {
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


