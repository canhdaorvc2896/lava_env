#!/bin/bash
if [ -f "board_config.txt" ]
then
        rm -rf board_config.txt
        touch board_config.txt
else
        touch board_config.txt
fi

model_name=`cat /proc/sys/kernel/hostname`

if [[ `egrep MMC /sys/block/mmcblk*/device/type` =~ "MMC" ]]
then
	mmc_flag=1
else
	mmc_flag=0
fi


if [ $mmc_flag == 1 ]
then
	if [ `egrep MMC /sys/block/mmcblk*/device/type` == "MMC" ]
	then
		mmc_check=`ls /sys/block/ | egrep mmc | egrep -v "boot|p"`	
	else 
		mmc_check=`egrep MMC /sys/block/mmcblk*/device/type | awk -F'/' '{print $4}'`
	fi
else
	if [ `egrep SD /sys/block/mmcblk*/device/type` == "SD" ]
	then
		mmc_check=`ls /sys/block/ | egrep mmc | egrep -v "boot|p"`	
	else 
		mmc_check=`egrep SD /sys/block/mmcblk*/device/type | awk -F'/' '{print $4}'`
	fi
fi

if [ $mmc_check == "mmcblk0" ]
then 
	if [ $model_name == "ek874" ]
	then
		sd0="mmcblk0"
		sd1="dont_exist"
		mmc="dont_exist"
	else
		mmc="mmcblk0"
		sd0="mmcblk1"
		sd1="mmcblk2"
	fi
fi


if [ $mmc_check == "mmcblk1" ]
then 
	mmc="mmcblk1"
	sd0="mmcblk0"
	sd1="mmcblk2"
fi


if [ $mmc_check == "mmcblk2" ]
then 
	mmc="mmcblk2"
	sd0="mmcblk0"
	sd1="mmcblk1"
fi


if [ $mmc_check == "" ]
then 
	sd0="mmcblk0"
	sd1="mmcblk1"
	mmc="dont_exist"
fi

sd0_count=`ls /dev/mmc* | egrep -w $sd0 | wc -l`
sd1_count=`ls /dev/mmc* | egrep -w $sd1 | wc -l`

if [ $sd0_count != "1" ]
then
	sd0="dont_exist"
fi

if [ $sd1_count != "1" ]
then
	sd1="dont_exist"
fi

if [ $mmc != "dont_exist" ]
then
	mmc_blk_size=`cat /sys/block/$mmc/queue/physical_block_size`
	total_block=`cat  /sys/block/$mmc/device/block/$mmc/size`
	mmc_blk_num=`expr $total_block - 4 - 4`
fi
if [ $sd0 != "dont_exist" ]
then
	sd0_blk_size=`cat /sys/block/$sd0/queue/physical_block_size`
	total_block=`cat  /sys/block/$sd0/device/block/$sd0/size`
	sd0_blk_num=`expr $total_block - 4 - 4`
fi
if [ $sd1 != "dont_exist" ]
then
	sd1_blk_size=`cat /sys/block/$sd1/queue/physical_block_size`
	total_block=`cat  /sys/block/$sd1/device/block/$sd1/size`
	sd1_blk_num=`expr $total_block - 4 - 4`
fi

echo -e "MMC_DEV = $mmc\t $mmc_blk_size\t $mmc_blk_num" >> board_config.txt
echo -e "SD0_DEV = $sd0\t $sd0_blk_size\t $sd0_blk_num" >> board_config.txt
echo -e "SD1_DEV = $sd1\t $sd1_blk_size\t $sd1_blk_num" >> board_config.txt



##Function: Detect MMC/SDHI information
#
#get_last_blks () {
#	# echo "get last blocks of $1"
#	local logical_blksize=''
#	local total_parts=''
#	local total_bytes=''
#	local blk_number=''
#
#	# check if device is available
#	if [ ! -e "/dev/${1}" ]
#	then
#		# echo "device not found!"
#		echo "N/A" 
#		return
#	fi	
#	
#	# get logical block size
#	logical_blksize=`lsblk -t /dev/${1} | sed -n 2p | awk '{print $6}'`
#
#	# get last 4 blocks number
#	## check:
#	## if device has no partition, get blocks of disk
#	## if device has partitions, get blocks of last's partition
#	total_parts=`lsblk -b /dev/${1} | egrep "part" -c`
#	
#	if [ $total_parts -eq 0 ]
#	then
#		total_bytes=`lsblk -b /dev/${1} | egrep "disk" | awk '{sum_bytes+=$4} END {printf "%.0f\n", sum_bytes}'`
#	elif [ $total_parts -gt 0 ]
#	then
#		total_bytes=`lsblk -b /dev/${1} | egrep "part" | awk '{sum_bytes+=$4} END {printf "%.0f\n", sum_bytes}'`
#	fi
#	
#	# echo "total_bytes $total_bytes"
#	# echo "logical_blksize $logical_blksize"
#	blk_number=`expr $total_bytes / $logical_blksize - 4`
#	echo -e "$logical_blksize\t $blk_number"
#}
