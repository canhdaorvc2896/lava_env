#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int get_Vendor_ID_USB_PCI(char input_device[10]){
	/* Get ID from device */
	FILE *file;
	char str[100];
	char changeToHex[6] = {'a', 'b', 'c', 'd', 'e', 'f'};
	int i, j;
	int value[10];
	int lengh, device_id = 0;
	/* open file to read ID device */
	file = fopen("./board_config.txt", "r");
	
	while (fscanf(file, "%s", str) != EOF) {
		/* check device and get ID */
		if(strcmp(str, input_device) == 0)
		{
			fscanf(file, "%s", str);
			fscanf(file, "%s", str);
			break;
		}
	}
	/* close file */
	fclose(file);
	/* get the lengh of ID */
	lengh = strlen(str);
	/* convert from string to hex value */
	if((str[0] == '0') && (str[1] == 'x')) {
		for (i = 2; i < lengh; i++)
		{
			if((str[i] >= '0') && (str[i] <= '9')) {
				value[i] = str[i] - 0x30;
			}
			else {
				for(j=0; j<6; j++) {
					if(str[i] == changeToHex[j]) {
						value[i] = j + 10;
					}
				}
			}
			device_id = device_id + value[i]*(pow(16, lengh - i - 1));
		}
	}
	
	return device_id;
}

char *get_ethernet_device(char input_device[]){
	/* Get ID from device */
	FILE *file;
	char *str = malloc(10*sizeof(char));
	/* open file to read ID device */
	file = fopen("./board_config.txt", "r");
	
	while (fscanf(file, "%s", str) != EOF) {
		/* check device and get ID */
		if(strcmp(str, input_device) == 0)
		{
			fscanf(file, "%s", str);
			fscanf(file, "%s", str);
			break;
		}
	}
	/* close file */
	fclose(file);
		
	return str; 
}

int get_block_number(char input_device[]){
	/* Get ID from device */
	FILE *file;
	char *str = malloc(10*sizeof(char));
	/* open file to read ID device */
	file = fopen("./board_config.txt", "r");
	
	while (fscanf(file, "%s", str) != EOF) {
		/* check device and get ID */
		if(strcmp(str, input_device) == 0)
		{
			fscanf(file, "%s", str);
			fscanf(file, "%s", str);
			fscanf(file, "%s", str);
			fscanf(file, "%s", str);
			break;
		}
	}
	/* close file */
	fclose(file);
	int block = atoi(str);		
	return block; 
}


int get_block_size(char input_device[]){
	/* Get ID from device */
	FILE *file;
	char *str = malloc(10*sizeof(char));
	/* open file to read ID device */
	file = fopen("./board_config.txt", "r");
	
	while (fscanf(file, "%s", str) != EOF) {
		/* check device and get ID */
		if(strcmp(str, input_device) == 0)
		{
			fscanf(file, "%s", str);
			fscanf(file, "%s", str);
			fscanf(file, "%s", str);
			break;
		}
	}
	/* close file */
	fclose(file);
	int block = atoi(str);		
	return block; 
}
