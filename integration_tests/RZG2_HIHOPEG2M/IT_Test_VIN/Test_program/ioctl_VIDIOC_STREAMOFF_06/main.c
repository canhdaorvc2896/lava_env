/*
* Project: CIP LAVA IT test
* Test ID: ioctl_VIDIOC_STREAMOFF_06
* Feature: Checking system call ioctl_VIDIOC_STREAMOFF of VIN driver
* Sequence: open() -> ioctl_VIDIOC_S_FMT() -> ioctl_VIDIOC_REQBUFS ->ioctl_VIDIOC_QUERYBUF -> mmap() -> ioctl_VIDIOC_STREAMON() -> ioctl_VIDIOC_QBUF() -> select() -> ioctl_VIDIOC_DQBUF() -> ioctl_VIDIOC_STREAMOFF()
* Testing level: API user lib
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_VIDIOC_STREAMOFF with device /dev/video0 to stop capturing image where type argument is NULL. Expected result = NG
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>


#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int result = -1;
struct buffer {
	void *start;
	size_t length;
};

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct v4l2_format format;
	struct v4l2_requestbuffers req_buf;
	struct v4l2_buffer buf;
	struct buffer *buffers;
	int i, mapped_buf_num;
	int type;
	fd_set rfds;
	struct timeval tv;

	/* Initialize variable and assign value for variable */
	memset (&format, 0, sizeof(format));
	
	/* Call API or system call follow describe in PCL */
	fd = open("/dev/video0", O_RDWR);
	
	/* Setting scaling & format */
	format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	format.fmt.pix.width			= 1280;
	format.fmt.pix.height			= 960;
	format.fmt.pix.pixelformat		= V4L2_PIX_FMT_UYVY;
	format.fmt.pix.field			= V4L2_FIELD_NONE;

	ioctl(fd, VIDIOC_S_FMT, &format);
	
	/* Request allocated buffers */
	req_buf.type				= V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req_buf.memory				= V4L2_MEMORY_MMAP;
	req_buf.count				= 2;
	
	ioctl(fd, VIDIOC_REQBUFS, &req_buf);
	
	buffers = calloc(req_buf.count, sizeof(*buffers));
	/* Query buffers */
	for(i = 0; i < req_buf.count; i++)
	{
		memset (&buf, 0, sizeof(buf));
		buf.type		= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory		= V4L2_MEMORY_MMAP;
		buf.index		= i;

		ioctl(fd, VIDIOC_QUERYBUF, &buf);
		buffers[i].length = buf.length;
		buffers[i].start = mmap(NULL, buf.length,
									PROT_READ | PROT_WRITE , MAP_SHARED, fd, buf.m.offset);
	}
	mapped_buf_num = i;
	
	/* Start Capturing */
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ioctl(fd, VIDIOC_STREAMON, &type);
	
	/* Enqueue frame buffers */
	for(i = 0; i < mapped_buf_num; i++)
	{
		memset (&buf, 0, sizeof(buf));
		buf.type		= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory		= V4L2_MEMORY_MMAP;
		buf.index		= i;
		
		ioctl(fd, VIDIOC_QBUF, &buf);
	}	
		/* Wait for the buffer to be filled up */
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);
		
		/* Timeout settings */
		tv.tv_sec = 5;
		tv.tv_usec = 0;
		
		select(fd + 1, &rfds, NULL, NULL, &tv);
		
		/* Dequeue frame buffers */
		memset (&buf, 0, sizeof(buf));
		buf.type		= V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory		= V4L2_MEMORY_MMAP;
		buf.index		= i;
		
		ioctl(fd, VIDIOC_DQBUF, &buf);
	
	/* Stop capturing image */
	result = ioctl(fd, VIDIOC_STREAMOFF, NULL);
	

	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	
	/* Close the device */
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

